package com.example.book_store.dto.auth;


import com.example.book_store.validation.Email;
import com.example.book_store.validation.PasswordMatches;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@PasswordMatches
public class UserRegDto {

    @NotNull
    @Email
    String email;

    @NotNull
    String fistName;

    @NotNull
    String lastName;

    @NotNull
    String password;

    @NotNull
    String prePassword;

}
