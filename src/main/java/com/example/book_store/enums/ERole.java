package com.example.book_store.enums;

public enum ERole {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_MODERATOR
}
