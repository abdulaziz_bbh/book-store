package com.example.book_store.model.auth;

import com.example.book_store.enums.ERole;
import com.example.book_store.model.BaseEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "role")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Role extends BaseEntity {

    ERole name;

    String description;

    public Role(ERole name){
        this.name = name;
    }
}
