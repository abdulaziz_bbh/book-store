package com.example.book_store.repository.auth;

import com.example.book_store.model.auth.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {
}
