package com.example.book_store.repository.auth;

import com.example.book_store.model.auth.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

}
