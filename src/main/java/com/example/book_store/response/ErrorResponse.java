package com.example.book_store.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {

    String errorMessage;
    int errorCode;

    String fieldKey;


    public ErrorResponse(String errorMessage, int errorCode){
        this.errorCode=errorCode;
        this.errorMessage=errorMessage;
    }
}
