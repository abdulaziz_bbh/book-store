package com.example.book_store.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResponseData<T> implements Serializable {

    T data;

    String message;

    ErrorResponse error;

    Boolean success;

    public ResponseData(Boolean success){
        this.success = success;
    }

    public ResponseData(T data){
        this.data = data;
        this.success = true;
    }

    public ResponseData(ErrorResponse error){
        this.error = error;
        this.success = false;
    }

}
