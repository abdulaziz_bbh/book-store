package com.example.book_store.validation;

import com.example.book_store.dto.auth.UserRegDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        UserRegDto userRegDto = (UserRegDto) object;
        return userRegDto.getPassword().equals(userRegDto.getPrePassword());
    }
}
